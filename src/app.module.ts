import { Module } from '@nestjs/common';
import { MailController } from './mail/mail.controller';
import { AppController } from './app.controller';

@Module({
  imports: [],
  controllers: [AppController, MailController],
  providers: [],
})
export class AppModule {}

import * as Imap from 'imap';

export const checkLive = (email: string, pwd: string) => {
  const imapConfig: Imap.Config = {
    user: email,
    password: pwd,
    host: 'outlook.office365.com',
    port: 993,
    tls: true,
    authTimeout: 25000,
    connTimeout: 30000,
  };
  const imap = new Imap(imapConfig);

  return new Promise((resolve) => {
    imap.once('ready', () => {
      imap.end();
      resolve({ status: true, message: 'Live Account' });
    });

    imap.once('error', () => {
      //console.log(err);
      resolve({
        status: false,
        message: `Cannot login to account or IMAP ins't working`,
      });
    });

    // imap.once('end', () => {
    //   //  console.log('end connection');
    // });

    imap.connect();
  });
};

import { checkLive } from './../utils/index';
import { Body, Controller, Get, HttpCode, Post } from '@nestjs/common';
import { MailDto } from './dto/mailDto';

@Controller('mail')
export class MailController {
  @Get()
  getMail() {
    return 'getMail';
  }
  @Post('check-live')
  @HttpCode(200)
  async checkEmailLive(@Body() mailDto: MailDto) {
    return await checkLive(mailDto.email, mailDto.pwd);
  }
}
